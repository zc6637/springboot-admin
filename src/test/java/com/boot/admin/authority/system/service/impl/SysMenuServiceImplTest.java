package com.boot.admin.authority.system.service.impl;

import cn.hutool.core.lang.tree.Tree;
import cn.hutool.json.JSONUtil;
import com.boot.admin.authority.system.service.SysMenuService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * description: SysMenuServiceImplTest <br>
 * date: 2020/5/11 10:01 <br>
 * author: 11752 <br>
 * version: 1.0 <br>
 */
@SpringBootTest
class SysMenuServiceImplTest {

    @Autowired
    private SysMenuService sysMenuService;

    @Test
    void findByMenu() {
        List<Tree<Integer>> trees = sysMenuService.findByMenu();
        String s = JSONUtil.parse(trees).toStringPretty();
        System.out.println(s);
        Assertions.assertNotNull(trees);
    }


}