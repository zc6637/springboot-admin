package com.boot.admin.authority.security.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.boot.admin.authority.security.jwt.util.JwtTokenUtil;
import com.boot.admin.authority.security.pojo.SysUser;
import com.boot.admin.authority.security.service.SysUserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * description: SysUserServiceImplTest <br>
 * date: 2020/5/12 11:22 <br>
 * author: 11752 <br>
 * version: 1.0 <br>
 */
@SpringBootTest
class SysUserServiceImplTest {

    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Test
    void findByUsername() {
        SysUser sysUser = sysUserService.findByUsername("admin");
        Assertions.assertNotNull(sysUser);
        Assertions.assertNotNull(sysUser.getSysRoles());
        Assertions.assertTrue(CollUtil.isNotEmpty(sysUser.getSysRoles()));
    }

}