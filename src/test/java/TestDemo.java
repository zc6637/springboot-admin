import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNode;
import cn.hutool.core.lang.tree.TreeNodeConfig;
import cn.hutool.core.lang.tree.TreeUtil;
import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;

/**
 * description: TestDemo <br>
 * date: 2020/5/12 9:32 <br>
 * author: 11752 <br>
 * version: 1.0 <br>
 */
public class TestDemo {


    @Test
    public void test() {
        PasswordEncoder delegatingPasswordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
        String admin = delegatingPasswordEncoder.encode("admin");
        System.out.println(admin);
        boolean admin1 = delegatingPasswordEncoder.matches("admin", admin);
        System.out.println(admin1);
    }

    @Test
    public void test1() {

    }


}
