package com.boot.admin.common.api;

/**
 * 封装API的错误码
 * Created by macro on 2019/4/19.
 */
public interface IErrorCode {
    /**
     * 状态码
     * @return
     */
    long getCode();

    /**
     * 信息
     * @return
     */
    String getMessage();
}