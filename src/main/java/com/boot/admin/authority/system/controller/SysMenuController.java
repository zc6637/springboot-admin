package com.boot.admin.authority.system.controller;

import cn.hutool.core.lang.tree.Tree;
import com.boot.admin.authority.system.service.SysMenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 菜单管理
 * @author zc
 */
@Api(tags = "系统菜单管理相关接口")
@RestController
@RequestMapping("/api")
public class SysMenuController {

    private final SysMenuService sysMenuService;

    public SysMenuController(SysMenuService sysMenuService) {
        this.sysMenuService = sysMenuService;
    }

    @ApiOperation(value = "查询并生成树状菜单接口")
    @GetMapping("/menu")
    public List<Tree<Integer>> menu() {
        return sysMenuService.findByMenu();
    }



}
