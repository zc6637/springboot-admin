package com.boot.admin.authority.system.controller;

import cn.hutool.json.JSON;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.boot.admin.authority.security.pojo.SysUser;
import com.boot.admin.common.api.CommonResult;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * description: SysUserController <br>
 * date: 2020/5/15 16:47 <br>
 * author: 11752 <br>
 * version: 1.0 <br>
 */
@RestController
@RequestMapping("/api")
public class SysUserController {

    @GetMapping("/userInfo")
    public CommonResult<JSONObject> currentUser() {
        SysUser sysUser = (SysUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        JSONObject jsonObject = JSONUtil.createObj().putOpt("name",sysUser.getUsername()).putOpt("avatar",sysUser.getAvatar());
        return CommonResult.success(jsonObject);
    }


}
