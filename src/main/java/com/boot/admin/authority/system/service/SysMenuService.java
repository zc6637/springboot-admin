package com.boot.admin.authority.system.service;

import cn.hutool.core.lang.tree.Tree;

import java.util.List;

/**
 * description: SysMenuService <br>
 * date: 2020/5/11 9:56 <br>
 * author: 11752 <br>
 * version: 1.0 <br>
 */
public interface SysMenuService {

    /**
     * 查询当前用户
     * @return 菜单数据
     */
    List<Tree<Integer>> findByMenu();

}
