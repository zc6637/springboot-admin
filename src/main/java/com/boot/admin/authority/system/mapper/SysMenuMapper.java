package com.boot.admin.authority.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.boot.admin.authority.system.pojo.SysMenu;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * description: SysMenuMapper <br>
 * date: 2020/5/11 9:55 <br>
 * author: 11752 <br>
 * version: 1.0 <br>
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    @Select("SELECT\n" +
            "\tm.* \n" +
            "FROM\n" +
            "\t`sys_menu` m\n" +
            "\tJOIN sys_role_menu rm ON m.menu_id = rm.menu_id \n" +
            "WHERE\n" +
            "\trm.role_id IN (${roleIds})" +
            "AND type IN (0,1) AND status = 0 " +
            "ORDER BY order_num")
    List<SysMenu> findByMenu(String roleIds);

}
