package com.boot.admin.authority.security.service;

import com.boot.admin.authority.security.pojo.SysUser;

/**
 * description: SysUserService <br>
 * date: 2020/5/12 10:33 <br>
 * author: 11752 <br>
 * version: 1.0 <br>
 */
public interface SysUserService {

    /**
     * 根据用户名查询用户数据
     * @param username 用户名
     * @return
     */
    SysUser findByUsername(String username);

}
