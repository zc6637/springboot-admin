package com.boot.admin.authority.security.pojo;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.io.Serializable;
import java.util.List;

/**
 * (SysUser)实体类
 *
 * @author makejava
 * @since 2020-05-13 17:47:45
 */
@Data
public class SysUser implements Serializable, UserDetails {
    private static final long serialVersionUID = -35688002901605844L;
    
    private Integer userId;
    /**
    * 用户名
    */
    private String username;
    /**
     * 昵称
     */
    private String name;
    /**
    * 密码
    */
    private String password;
    
    private Long deptId;
    /**
    * 邮箱
    */
    private String email;
    /**
    * 手机号
    */
    private String mobile;
    /**
     * 用户头像地址
     */
    private String avatar;
    /**
    * 状态 0:禁用，1:正常
    */
    private Object status;
    /**
    * 创建用户id
    */
    private Long userIdCreate;
    /**
    * 创建时间
    */
    private Date createDate;
    /**
    * 修改时间
    */
    private Date updateDate;
    /**
    * 性别
    */
    private Object sex;
    /**
    * 出身日期
    */
    private Date birth;
    
    private Integer picId;
    /**
    * 现居住地
    */
    private String liveAddress;
    /**
    * 爱好
    */
    private String hobby;
    /**
    * 省份
    */
    private String province;
    /**
    * 所在城市
    */
    private String city;
    /**
    * 所在地区
    */
    private String district;

    /**
     * 当前用户都角色
     */
    private List<SysRole> sysRoles;


    /**
     * 账户是否未过期，
     * true： (即未过期)
     * false：(即过期)
     */
    private boolean accountNonExpired;

    /**
     * 指示用户是否被锁定或解锁。锁定的用户不能认证。
     * true：用户没有被锁定
     * false：否则锁定
     */
    private boolean accountNonLocked;

    /**
     * 指示用户的凭据（密码）是否已过期
     * true：如果用户的凭据是有效的(即未过期)，
     * false：如果不再有效(即过期)
     */
    private boolean credentialsNonExpired;

    /**
     * 指示用户是启用还是禁用。
     * true：用户已启用
     * false：用户已禁用
     */
    private boolean enabled;


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        for (SysRole role : getSysRoles()) {
            authorities.add(new SimpleGrantedAuthority(role.getRoleSign()));
        }
        return authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }
}