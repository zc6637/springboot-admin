package com.boot.admin.authority.security.service.impl;

import com.boot.admin.authority.security.pojo.SysUser;
import com.boot.admin.authority.security.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * description: JwtUserDetailsServiceImpl <br>
 * date: 2020/5/11 21:25 <br>
 * author: 11752 <br>
 * version: 1.0 <br>
 */
@Service
public class JwtUserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private SysUserService sysUserService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        SysUser sysUser = sysUserService.findByUsername(username);
        if (sysUser == null) {
            throw new UsernameNotFoundException(String.format("%s.用户不存在", username));
        }
        return sysUser;
    }
}
