package com.boot.admin.authority.security.service.impl;

import com.boot.admin.authority.security.mapper.SysUserMapper;
import com.boot.admin.authority.security.pojo.SysUser;
import com.boot.admin.authority.security.service.SysUserService;
import org.springframework.stereotype.Service;

/**
 * description: SysUserServiceImpl <br>
 * date: 2020/5/12 10:33 <br>
 * author: 11752 <br>
 * version: 1.0 <br>
 */
@Service
public class SysUserServiceImpl implements SysUserService {

    private final SysUserMapper sysUserMapper;

    public SysUserServiceImpl(SysUserMapper sysUserMapper) {
        this.sysUserMapper = sysUserMapper;
    }

    @Override
    public SysUser findByUsername(String username) {
        SysUser sysUser = sysUserMapper.findByUsername(username);
        return sysUser;
    }
}
