package com.boot.admin.authority.security.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.boot.admin.authority.security.pojo.SysUser;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * description: SysUserMapper <br>
 * date: 2020/5/12 10:34 <br>
 * author: 11752 <br>
 * version: 1.0 <br>
 */
public interface SysUserMapper extends BaseMapper<SysUser> {


    /**
     * 根据登录的用户名名查询
     * @param username 账户名
     * @return
     */
    @Select("SELECT * FROM sys_user WHERE username = #{username}")
    @Results({
            @Result(property = "username",column = "username"),
            @Result(property = "sysRoles", column = "user_id", javaType = List.class,
                    many = @Many(select = "com.boot.admin.authority.security.mapper.SysRoleMapper.findByUserId"))
    })
    SysUser findByUsername(String username);

}
