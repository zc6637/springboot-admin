package com.boot.admin.authority.security.jwt.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * description: UserLogin <br>
 * date: 2020/5/12 21:37 <br>
 * author: 11752 <br>
 * version: 1.0 <br>
 */
@Data
public class UserLogin implements Serializable {

    private String userName;
    private String password;
    private String type;

}
