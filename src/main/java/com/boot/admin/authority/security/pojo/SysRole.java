package com.boot.admin.authority.security.pojo;

import lombok.Data;

import java.util.Date;
import java.io.Serializable;

/**
 * 角色(SysRole)实体类
 *
 * @author makejava
 * @since 2020-05-13 17:47:52
 */
@Data
public class SysRole implements Serializable {
    private static final long serialVersionUID = 327983183656538503L;
    
    private Integer roleId;
    /**
    * 角色名称
    */
    private String roleName;
    /**
    * 角色标识
    */
    private String roleSign;
    /**
    * 备注
    */
    private String remark;
    /**
    * 创建用户id
    */
    private Integer userIdCreate;
    /**
    * 创建时间
    */
    private Date createDate;
    /**
    * 修改时间
    */
    private Date updateDate;

}