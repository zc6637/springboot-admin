package com.boot.admin.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author zc
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket creatRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .pathMapping("/")
                .select()
                // 方法需要有ApiOperation注解才能生存接口文档
                // .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                // 默认 api所在的package 都生成
                .apis(RequestHandlerSelectors.basePackage("com.boot.admin.authority.system.controller"))
                // 页面风格
                .paths(PathSelectors.any())
                // 运行并初始化相关内容
                .build().apiInfo(new ApiInfoBuilder()
                        .title("springboot-admin后台管理系统")
                        .version("1.0")
                        .description("详细信息")
                        .contact(new Contact("邮箱", "https://www.csdn.net/", "1175250848@qq.com"))
                        .licenseUrl("http://www.javaboy.org")
                        .build());

    }

}
